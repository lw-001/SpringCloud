package com.study.springcloud.dao;

import com.study.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/9
 * @Description: com.study.springcloud.dao
 * @Version: 1.0
 *
 * @Mapper:表示该接口类的实现类对象交给mybatis底层创建，然后交由Spring框架管理。
 * @Repository:它用于将数据访问层 (DAO 层 ) 的类标识为 Spring Bean
 */
@Mapper
@Repository
public interface DeptDAO {

    Boolean addDept(Dept dept);

    Dept queryById(Long id);

    List<Dept> queryAll();
}
