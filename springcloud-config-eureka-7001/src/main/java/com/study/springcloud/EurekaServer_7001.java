package com.study.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/15
 * @Description: com.study.springcloud
 * @Version: 1.0
 *
 * 启动之后访问http://localhost:7001
 */
@SpringBootApplication
@EnableEurekaServer//Eureka服务端的启动类，可以接受别人注册进来
public class EurekaServer_7001 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServer_7001.class,args);
    }
}
