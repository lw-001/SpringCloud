package com.study.springcloud.service;

import com.study.springcloud.pojo.Dept;

import java.util.List;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/9
 * @Description: com.study.springcloud.service
 * @Version: 1.0
 */
public interface DeptService {

    Boolean addDept(Dept dept);

    Dept queryById(Long id);

    List<Dept> queryAll();
}
