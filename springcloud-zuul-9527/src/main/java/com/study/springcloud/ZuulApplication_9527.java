package com.study.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/4/2
 * @Description: com.study.springcloud
 * @Version: 1.0
 */
@SpringBootApplication
@EnableZuulProxy//开启zuul代理
public class ZuulApplication_9527 {

    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication_9527.class,args);
    }
}
