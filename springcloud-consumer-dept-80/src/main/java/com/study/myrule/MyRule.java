package com.study.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/28
 * @Description: com.study.myrule
 * @Version: 1.0
 */
@Configuration
public class MyRule {

    @Bean
    public IRule myRule(){
        //默认是轮询，现在改为自定义的负载均衡算法
        return new MyRandomRule();
    }
}
