package com.study.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/4/7
 * @Description: com.study.springcloud
 * @Version: 1.0
 */
@SpringBootApplication
public class ConfigClient_3355 {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClient_3355.class,args);
    }
}
