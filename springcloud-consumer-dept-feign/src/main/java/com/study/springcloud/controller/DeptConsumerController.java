package com.study.springcloud.controller;

import com.study.springcloud.pojo.Dept;
import com.study.springcloud.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/10
 * @Description: com.study.springcloud.controller
 * @Version: 1.0
 */
@RequestMapping("/consumer/dept")
@RestController
public class DeptConsumerController {


    @Autowired
    private DeptClientService service=null;

    @GetMapping("/add")
    public Boolean add(Dept dept) {
       return this.service.addDept(dept);
    }

    @GetMapping("/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        return this.service.queryById(id);
    }

    @GetMapping("/list")
    public List<Dept> list() {
       return this.service.queryAll();
    }


}
