package com.study.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/10
 * @Description: com.study.springcloud.config
 * @Version: 1.0
 *
 * @Configuration:相当于Spring的applicationContext.xml
 */
@Configuration
public class ConfigBean {


    @Bean
    /**
     * Ribbon    配置负载均衡实现RestTemplate
     *
     * TRule
     * RoundRobinRule   轮询
     * RandomRule       随机
     * AvailabilityFilteringRule    会先过滤掉跳闸/访问故障的服务，对剩下的进行轮询
     * RetryRule        会先按照轮询获取服务，如果服务获取失败，则会在指定的时间内进行重试
     */
    @LoadBalanced
    public RestTemplate getTemplate(){
        return new RestTemplate();
    }


}
