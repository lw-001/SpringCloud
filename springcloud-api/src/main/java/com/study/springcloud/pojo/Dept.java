package com.study.springcloud.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Auther: LW
 * @Date: 2022/3/7
 * @Description: com.lw.springcloud.pojo
 * @version: 1.0
 *
 * 实体类：orm  类表关系映射
 */
@Data//提供类的get、set、equals、hashCode、canEqual、toString方法
@NoArgsConstructor//无参构造器
@Accessors(chain = true)//链式写法
public class Dept implements Serializable {

    /**
     * 主键id
     */
    private Long deptNo;

    /**
     * 部门名称
     */
    private String dName;

    /**
     * 数据来源：这个数据存在哪个数据库的字段
     * 微服务，一个服务对应一个数据库，同一个信息可能存在
     * 不同的数据库中
     */
    private String dbSource;


    public Dept(String dName) {
        this.dName = dName;
    }

    public static void main(String[] args){

    }


    /*
    链式写法：
       Dept dept = new Dept();
       dept.setDeptNo(11L).setDName("sss").setDbSource("db1");
     */
}
