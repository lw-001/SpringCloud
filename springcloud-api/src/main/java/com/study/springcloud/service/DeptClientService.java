package com.study.springcloud.service;

import com.study.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/29
 * @Description: com.study.springcloud.service
 * @Version: 1.0
 */
@Component
@FeignClient(value = "RCN_REDUNDANT_NULLCHECK_OF_NULL_VALUE",fallbackFactory = DeptClientServiceFallbackFactory.class)
public interface DeptClientService {

    @GetMapping("/dept/get/{id}")
    Dept queryById(@PathVariable("id") Long id);

    @GetMapping("/dept/list")
    List<Dept> queryAll();

    @PostMapping("/dept/add")
    Boolean addDept(Dept dept);
}
