package com.study.springcloud.service;

import com.study.springcloud.pojo.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/4/1
 * @Description: com.study.springcloud.service
 * @Version: 1.0
 *
 * 降级
 */
@Component
public class DeptClientServiceFallbackFactory implements FallbackFactory {
    public DeptClientService create(Throwable throwable) {
        return new DeptClientService() {
            public Dept queryById(Long id) {
                return new Dept().setDeptNo(id).setDName("id=>"+id+"没有对应的信息，客户端提供了降级端信息，这个服务现在已经被关闭").setDbSource("没有数据");
            }

            public List<Dept> queryAll() {
                return null;
            }

            public Boolean addDept(Dept dept) {
                return null;
            }
        };
    }
}
