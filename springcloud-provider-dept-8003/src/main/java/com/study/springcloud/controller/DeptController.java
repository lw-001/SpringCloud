package com.study.springcloud.controller;

import com.study.springcloud.pojo.Dept;
import com.study.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/9
 * @Description: com.study.springcloud.controller
 * @Version: 1.0
 * <p>
 * 提供Restful服务
 */
@RequestMapping("/dept")
@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;

    /**
     * 获取一些配置的信息，得到具体的微服务
     */
    @Autowired
    private DiscoveryClient discoveryClient;

    @PostMapping("/add")
    public Boolean addDept(Dept dept) {
        return deptService.addDept(dept);
    }

    @GetMapping("/get/{id}")
    public Dept queryById(@PathVariable("id") Long id) {
        return deptService.queryById(id);
    }

    @GetMapping("/list")
    public List<Dept> queryAll() {
        return deptService.queryAll();
    }

    /**
     * 注册进来的微服务，获取一些消息
     *
     * @return
     */
    @GetMapping("/discovery")
    public Object discovery() {
        //获取微服务列表的清单
        List<String> services = discoveryClient.getServices();
        System.out.println("discovery-->services:" + services);

        //得到一个具体的微服务信息，通过具体的微服务id(applicationName)
        List<ServiceInstance> instances = discoveryClient.getInstances("SPRINGCLOUD-PROVIDER-DEPT");

        for (ServiceInstance instance : instances) {
            System.out.println(
                    instance.getHost() + "\t" +
                            instance.getPort() + "\t" +
                            instance.getUri() + "\t" +
                            instance.getServiceId()
            );
        }

        return this.discoveryClient;
    }

}
