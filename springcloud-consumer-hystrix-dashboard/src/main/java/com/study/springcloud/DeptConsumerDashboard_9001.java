package com.study.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/4/2
 * @Description: com.study.springcloud
 * @Version: 1.0
 */
@SpringBootApplication
@EnableHystrixDashboard//开启
public class DeptConsumerDashboard_9001 {

    public static void main(String[] args) {
        SpringApplication.run(DeptConsumerDashboard_9001.class,args);
    }
}
