package com.study.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/17
 * @Description: com.study.springcloud
 * @Version: 1.0
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer_7003 { 
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer_7003.class,args);
    }
}

