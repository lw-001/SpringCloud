package com.study.springcloud.service.impl;

import com.study.springcloud.dao.DeptDAO;
import com.study.springcloud.pojo.Dept;
import com.study.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: 鼠宣伟
 * @Date: 2022/3/9
 * @Description: com.study.springcloud.service.impl
 * @Version: 1.0
 */
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDAO deptDAO;

    public Boolean addDept(Dept dept) {
        return deptDAO.addDept(dept);
    }

    public Dept queryById(Long id) {
        return deptDAO.queryById(id);
    }

    public List<Dept> queryAll() {
        return deptDAO.queryAll();
    }
}
